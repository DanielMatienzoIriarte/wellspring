<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>wellspring</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="resources/css/wellspring.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="" defer></script>
</head>
<body>
    <div id="container">
        <div id="top-menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="resources/views/uploadCSV.php">Upload CSV</a></li>
            </ul>
        </div>
        <div class="header">
            <h1>Welcome to Wellspring Trains Route Manager</h1>
        </div>
    </div>
</body>
</html>