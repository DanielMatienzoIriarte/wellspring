<?php
namespace wellspring\app\services;

use wellspring\app\Models\TrainRoute;

interface TrainRouteManagerInterface
{
    /**
     * Gets train route information from a source and returns it as an ordered array.
     * @param string $name
     * @return array
     */
    public function import(string $name): array;

    /**
     * Returns train routes information from a source and returns it as a TrainRoute Object.
     * @param string $name
     * @return TrainRoute
     */
    public function getTrainRouteData(string $name): TrainRoute;
}
