<?php
namespace wellspring\app\services;

interface HydratorInterface
{
    public function hydrate(array $data);
}
