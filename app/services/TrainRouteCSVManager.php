<?php
namespace wellspring\app\services;

use wellspring\app\Models\TrainRoute;

class TrainRouteCSVManager implements TrainRouteManagerInterface
{
    private HydratorInterface $trainRouteHydrator;

    /**
     * TrainRouteCSVManager constructor.
     * @param TrainRouteHydrator $trainRouteHydrator
     */
    public function __construct(HydratorInterface $trainRouteHydrator) {
        $this->trainRouteHydrator = $trainRouteHydrator;
    }

    /**
     * {@inheritDoc}
     */
    public function import(string $name): array
    {
        $result = [];

        $routes = $this->readCsvFile($name);
        $routes = $this->sortRoutesByRun($routes);

        foreach ($routes as $route) {
            $result[] = $this->trainRouteHydrator->hydrate($route);
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function getTrainRouteData(string $name): TrainRoute
    {
        return $this->hydrateRoutes($this->readCsvFile($name));
    }

    /**
     * Reads a CSV file and returns an array with its data.
     * @param string $fileName
     * @return array
     */
    private function readCsvFile(string $fileName): array
    {
        $csv = array_map('str_getcsv', file($fileName));
        array_shift($csv);

        return $csv;
    }

    /**
     * Receives an array and returns a TrainRoute object fully filled with train routes.
     * @param array $routesData
     * @return TrainRoute
     */
    private function hydrateRoutes(array $routesData): TrainRoute
    {
        return $this->trainRouteHydrator->hydrate($routesData);
    }

    /**
     * Sort the array of routes considering the Run Number as pivot and returns sorted array.
     * @param array $routes
     * @return array
     */
    private function sortRoutesByRun(array $routes): array
    {
        $routes = array_unique($routes, SORT_REGULAR);

        usort($routes, function($a, $b) {
            return $a[2] <=> $b[2];
        });

        return $routes;
    }
}
