<?php
namespace wellspring\app\services;

interface TrainRouteControllerInterface
{
    /**
     * Takes the path and filename of a CSV and returns an array with the routes to be displayed.
     * @param string $fileName
     * @return mixed
     */
    public function displayRoutes(string $fileName);
}
