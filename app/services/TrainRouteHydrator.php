<?php
namespace wellspring\app\services;

use wellspring\app\Models\TrainRoute;

class TrainRouteHydrator implements HydratorInterface
{
    public function hydrate(array $data): TrainRoute
    {
        $trainRoute = new TrainRoute();

        if (!empty($data)) {
            $trainRoute->setLine($data[0]);
            $trainRoute->setRoute($data[1]);
            $trainRoute->setRunNumber($data[2]);
            $trainRoute->setOperatorId($data[3]);
        }

        return $trainRoute;
    }
}
