<?php
namespace wellspring\app\controllers;

use wellspring\app\services\TrainRouteControllerInterface;
use wellspring\app\services\TrainRouteManagerInterface;

class TrainRouteController implements TrainRouteControllerInterface
{
    private TrainRouteManagerInterface $trainRouteCvsManager;

    /**
     * TrainRouteController constructor.
     * @param TrainRouteManagerInterface $trainRouteCvsManager
     */
    public function __construct(TrainRouteManagerInterface $trainRouteCvsManager)
    {
        $this->trainRouteCvsManager = $trainRouteCvsManager;
    }

    /**
     * {@inheritDoc}
     */
    public function displayRoutes(string $fileName): array
    {
        return $this->trainRouteCvsManager->import($fileName);
    }
}
