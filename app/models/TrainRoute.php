<?php
namespace wellspring\app\models;

class TrainRoute
{
    /**
     * @var string
     */
    private string $line;

    /**
     * @var string
     */
    private string $route;

    /**
     * @var string
     */
    private string $runNumber;

    /**
     * @var string
     */
    private string $operatorId;

    /**
     * @return string
     */
    public function getLine(): string
    {
        return $this->line;
    }

    /**
     * @param string $line
     */
    public function setLine(string $line): void
    {
        $this->line = $line;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getRunNumber(): string
    {
        return $this->runNumber;
    }

    /**
     * @param string $runNumber
     */
    public function setRunNumber(string $runNumber): void
    {
        $this->runNumber = $runNumber;
    }

    /**
     * @return string
     */
    public function getOperatorId(): string
    {
        return $this->operatorId;
    }

    /**
     * @param string $operatorId
     */
    public function setOperatorId(string $operatorId): void
    {
        $this->operatorId = $operatorId;
    }
}