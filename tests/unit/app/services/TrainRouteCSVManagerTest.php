<?php
namespace unit\app\services;

use PHPUnit\Framework\TestCase;
use wellspring\app\models\TrainRoute;
use wellspring\app\services\TrainRouteCSVManager;
use wellspring\app\services\TrainRouteHydrator;
use wellspring\app\services\TrainRouteManagerInterface;

class TrainRouteCSVManagerTest extends TestCase
{
    private TrainRouteManagerInterface $trainRouteCSVManager;

    public function __construct()
    {
        parent::__construct();

        $trainRoute = new TrainRoute();
        $trainRouteHydratorMock = $this->getMockBuilder(TrainRouteHydrator::class)
            ->onlyMethods(['hydrate'])
            ->getMock();
        $trainRouteHydratorMock->method('hydrate')->willReturn($trainRoute);

        $this->trainRouteCSVManager = new TrainRouteCSVManager($trainRouteHydratorMock);
    }

    public function testImport() {
        $trainRoute = ['Amtrak', 'Hiawatha', 'A005', 'LBeck'];

        $result = $this->trainRouteCSVManager->import('file.csv');

        $this->assertNotNull($result);
    }
}
