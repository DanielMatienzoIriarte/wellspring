<?php
namespace unit\app\services;

use PHPUnit\Framework\TestCase;
use wellspring\app\services\HydratorInterface;
use wellspring\app\Services\TrainRouteHydrator;

require_once __DIR__ . '/../../../../app/models/TrainRoute.php';
require_once __DIR__ . '/../../../../app/services/HydratorInterface.php';
require_once __DIR__ . '/../../../../app/services/TrainRouteHydrator.php';

class TrainRouteHydratorTest extends TestCase
{
    private HydratorInterface $hydrator;

    public function __construct()
    {
        parent::__construct();

        $this->hydrator = new TrainRouteHydrator();
    }

    public function testHydrateNotEmptyRoutes(): void
    {
        $trainRoute = ['Amtrak', 'Hiawatha', 'A005', 'LBeck'];

        $trainRouteModel = $this->hydrator->hydrate($trainRoute);

        $this->assertEquals($trainRoute[0], $trainRouteModel->getLine());
    }

    public function testHydrateEmptyRoutes(): void
    {
        $trainRoute = [];

        $trainRouteModel = $this->hydrator->hydrate($trainRoute);

        $this->assertNotNull($trainRouteModel);
    }
}
