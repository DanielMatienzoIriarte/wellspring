<?php
namespace unit\app\controller;

use PHPUnit\Framework\TestCase;
use wellspring\app\controllers\TrainRouteController;
use wellspring\app\models\TrainRoute;
use wellspring\app\services\TrainRouteCSVManager;
use wellspring\app\services\TrainRouteManagerInterface;

class TrainRouteControllerTest extends TestCase
{
    private TrainRouteController $trainRouteController;
    private TrainRouteManagerInterface $trainRouteManager;

    /**
     * TrainRouteControllerTest constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $trainRoute = new TrainRoute();

        $this->trainRouteManager = $this->getMockBuilder(TrainRouteCSVManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['import'])
            ->getMock();
        $this->trainRouteManager->method('import')->willReturn([$trainRoute]);
    }

    /**
     * Tests the controller's correct functionality by asserting that its result has an instance of the expected model.
     */
    public function testDisplayRoutes()
    {
        $this->trainRouteController = new TrainRouteController($this->trainRouteManager);

        $result = $this->trainRouteController->displayRoutes('filename');
        $this->assertInstanceOf(TrainRoute::class, $result[0]);
    }
}
