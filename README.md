Wellspring
Train Route Display

Directions:
* Clone the repository.
* Install libraries by running composer.
* Make resources/files folder writable (777)
* To run tests use the following command:
  
  vendor/bin/phpunit --bootstrap vendor/autoload.php --coverage-html tests/coverage {tests/unit/test name}
  
* Coverage is visible by opening index.html from tests/coverage folder.

Considerations:
* The system has been built using PHP 7.4 core, no frameworks nor layout template managers.
* The system uploads a csv file and displays the train routes sorting them by Run Number, repeated Run Numbers are removed.
* I have followed MVC when creating the system, index.php file is the main entrance point.

Thoughts:
* Due the limited timeframe I couldn't complete all that was asked.
* index.php and resources/views/uploadCSV.php files have the same head and top menu, they should be put on one single template and be reused.
* Basic css is being used instead of using tailwind or bootstrap, so styling is very easy, SASS would be great to use but I've been here for 6 hours already.
* While performing unit test for TrainRouteCSVManager service I realized that it has to be refactored and the CSV file import function taken to a different class in order to keep Single Responsibility and increase Dependency Injection.
* Resources/views/uploadCSV.php file is currently creating the controller, which should be done via Factory, but no time to create another class and test it.
