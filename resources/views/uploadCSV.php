<?php

use wellspring\app\controllers\TrainRouteController;
use wellspring\app\services\TrainRouteCSVManager;
use wellspring\app\services\TrainRouteHydrator;

require_once '../../app/services/TrainRouteControllerInterface.php';
require_once '../../app/services/HydratorInterface.php';
require_once '../../app/services/TrainRouteManagerInterface.php';
require_once '../../app/models/TrainRoute.php';
require_once '../../app/services/TrainRouteHydrator.php';
require_once '../../app/services/TrainRouteCSVManager.php';
require_once '../../app/controllers/TrainRouteController.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>wellspring</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="../css/wellspring.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="" defer></script>
</head>
<body>
<div id="container">
    <div id="top-menu">
        <ul>
            <li><a href="../../index.php">Home</a></li>
            <li><a href="../views/uploadCSV.php">Upload CSV</a></li>
        </ul>
    </div>
<?php
if (isset($_POST['submit'])) {
    $baseName = basename($_FILES["fileToUpload"]["name"]);
    $target_dir = __DIR__ . "/../files/";
    $target_file = $target_dir . $baseName;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $trainRouteHydrator = new TrainRouteHydrator();
        $trainRoutCSVManager = new TrainRouteCSVManager($trainRouteHydrator);
        $controllerObj = new TrainRouteController($trainRoutCSVManager);
        $trainRoutes = $controllerObj->displayRoutes($target_file);
//        print_r($trainRoutes);
        ?>
            <table class="routesTable">
                <thead>
                    <tr>
                        <th>
                            Train Line
                        </th>
                        <th>
                            Route
                        </th>
                        <th>
                            Run Number
                        </th>
                        <th>
                            Operator ID
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($trainRoutes as $trainRoute) {
                            echo '<tr>';
                                echo '<td>';
                                echo $trainRoute->getLine();
                                echo '</td >';
                                echo '<td>';
                                echo $trainRoute->getRoute();
                                echo '</td >';
                                echo '<td>';
                                echo $trainRoute->getRunNumber();
                                echo '</td >';
                                echo '<td>';
                                echo $trainRoute->getOperatorId();
                                echo '</td >';
                            echo '</tr >';
                        }?>
                </tbody>
            </table>
        <?php
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
} else {
    ?>

    <form action="" method="post" enctype="multipart/form-data">
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Upload CSV File" name="submit">
    </form>

    <?php
}
?>
    </div>
</body>
</html>